"""In this file I define a class that will act as a node of the symbol table"""

class SymbolTable(object):
    """The symbol table class that will act as a binary tree and a linked list"""

    def __init__(self, _value='', _unique_id=''):
        self.value = _value
        self.unique_id = _unique_id

        # Initiates the left and right nodes with None values since the node has just been created.
        self.left_node = None
        self.right_node = None
    
    def __str__(self):
        """turns the symbol table into a string"""
        out_str = '[\'' + self.unique_id + '\', \'' + self.value + '\'] '

        # If there's a node on the left, make the left node's string first
        if self.left_node:
            out_str = str(self.left_node) + out_str
        
        # If there's a node on the right, make the right node's string last
        if self.right_node:
            out_str += str(self.right_node)
        
        return out_str
    
    # Takes either a value to search for or takes a unique_id to search the symbol table for
    def find(self, value):
        """Find a node in the symbol table"""

        # If the function has been misused, this will catch it
        if not value:
            raise Exception('Expected a value or unique_id to find in the symbol table.')

        # If the method has found the desired value
        if self.value == value:
            return self
        # Searching through the binary tree, if the desired value is greater than, the right node's find will be called and vice versa
        elif self.right_node and value > self.value:
            return self.right_node.find(value)
        elif self.left_node and value < self.value:
            return self.left_node.find(value)

    # Takes both a value and unique_id to create a node
    def create(self, value, unique_id):
        """Create a node in the symbol table"""

        # If the function has been misused, this will catch it
        if not (value or unique_id):
            raise Exception('Expected a value or unique_id to find in the symbol table.')
        # If the entry already exists in the symbol table, it will error
        elif self.value == value:
            raise Exception('Tried creating an entry to the symbol table that already exists.')

        # If the value to be created is greater than self.value, it will choose the right_node, else the left_node
        if value > self.value:
            # If the right_node exists, it will run it's create method
            if self.right_node:
                self.right_node.create(value, unique_id)
            # If the right_node doesn't exist, it will create a new instance of SymbolTable
            else:
                self.right_node = SymbolTable(value, unique_id)
        else:
            if self.left_node:
                self.left_node.create(value, unique_id)
            else:
                self.left_node = SymbolTable(value, unique_id)
