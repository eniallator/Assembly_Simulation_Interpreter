"""Anything and everything about the assembly language's instructions"""
from src.Instructions.RamGeneration import GEN_RAM
from src.Instructions.RunTimeMethods import RUN_TIME_METHOD

INSTRUCTION_DATA = [
    {
        # The instruction mnemonic
        'name': 'ADD',
        
        # The function used to generate the starting memory for this instruction
        'gen_ram': GEN_RAM['default'],

        # Describes the syntax for the instruction
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': False
            }
        }
    }, {
        'name': 'SUB',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': False
            }
        }
    }, {
        'name': 'NOT',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            }
        }
    }, {
        'name': 'BOR',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': False
            }
        }
    }, {
        'name': 'AND',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': False
            }
        }
    }, {
        'name': 'XOR',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': False
            }
        }
    }, {
        'name': 'LSH',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': True
            }
        }
    }, {
        'name': 'RSH',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': True
            }
        }
    }, {
        'name': 'END',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            }
        }
    }, {
        'name': 'BRA',
        'gen_ram': GEN_RAM['branch'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'ba',
                'optional': False
            }
        }
    }, {
        'name': 'BRP',
        'gen_ram': GEN_RAM['branch'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'ba',
                'optional': False
            }
        }
    }, {
        'name': 'BRZ',
        'gen_ram': GEN_RAM['branch'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'ba',
                'optional': False
            }
        }
    }, {
        'name': 'LDA',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'cva',
                'optional': False
            }
        }
    }, {
        'name': 'STA',
        'gen_ram': GEN_RAM['sta'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            },
            'right': {
                'type': 'va',
                'optional': False
            }
        }
    }, {
        'name': 'INP',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            }
        }
    }, {
        'name': 'OUT',
        'gen_ram': GEN_RAM['default'],
        'syntax': {
            'left': {
                'type': 'b',
                'optional': True
            }
        }
    }, {
        'name': 'VAR',
        'gen_ram': GEN_RAM['var'],
        'syntax': {
            'left': {
                'type': 'v',
                'optional': False
            },
            'right': {
                'type': 'c',
                'optional': True
            }
        }
    }
]

INSTRUCTION_NAME_IDS = [instruction['name'] for instruction in INSTRUCTION_DATA]
