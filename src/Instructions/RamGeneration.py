"""Methods that get called when loading instructions into ram"""
from src.BinaryConversion import num_to_bin


def _get_operand_value(operand, ref_dict):
    """Gets the operand's value from either the reference list or if it's an address type"""
    if operand in ref_dict:
        return ref_dict[operand]
    return int(operand[1:]) - 1


GEN_RAM = {}


def _gen_var(operator, operand, var_ref, branch_ref):
    """Converts a constant given as the variable value to it's binary equivalent"""
    if type(operand) is not str:
        operand = 'c0'
    ram_contents = num_to_bin(int(operand[1:]), True, length=16)
    return ram_contents

GEN_RAM['var'] = _gen_var


def _gen_sta(operator, operand, var_ref, branch_ref):
    """Generates the sta instruction's ram contents"""
    # Creating the operator value as the instruction id for the first 4 bits
    ram_contents = num_to_bin(int(operator[1:]), False, length=4)

    # Adding the variable's location reference to the ram contents
    ram_contents += num_to_bin(_get_operand_value(operand, var_ref), False, length=12)

    return ram_contents

GEN_RAM['sta'] = _gen_sta


def _gen_branch(operator, operand, var_ref, branch_ref):
    """Generates the branch instruction's ram contents"""
    # Creating the operator value as the instruction id for the first 4 bits
    ram_contents = num_to_bin(int(operator[1:]), False, length=4)

    # Making a reference to the line number that the branch points towards
    ram_contents += num_to_bin(_get_operand_value(operand, branch_ref), False, length=12)
    return ram_contents

GEN_RAM['branch'] = _gen_branch


def _gen_default(operator, operand, var_ref, branch_ref):
    """Generates the default instruction's ram contents"""
    # Defaulting the operand as a constant with a value of 0
    if type(operand) is not str:
        operand = 'c0'
    
    # Creating the operator value as the instruction id for the first 4 bits
    ram_contents = num_to_bin(int(operator[1:]), False, length=4)

    if operand.startswith('v') or operand.startswith('a'):
        # Creating the control bit
        ram_contents += '1'

        # Adding the binary line number on to the ram contents
        ram_contents += num_to_bin(_get_operand_value(operand, var_ref), False, length=11)
    else:
        # Creating the control bit
        ram_contents += '0'

        # Adding the constant's binary value to the ram contents
        ram_contents += num_to_bin(int(operand[1:]), True, length=11)
    
    return ram_contents

GEN_RAM['default'] = _gen_default
