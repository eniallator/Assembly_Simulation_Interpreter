"""The methods that get called in run-time for instructions"""
from src.BinaryConversion import num_to_bin, bin_to_num


def _get_val(operand, ram):
    """Gets the value when there is a control bit first"""
    value = operand[1:]
    if operand.startswith('1'):
        ram_address = bin_to_num(value, False)
        value = ram[ram_address]
    return bin_to_num(value, True)

def _overlap_accumulator(num):
    """Makes sure that the result of an operation is within the accumulator's range"""
    while not (- 2 ** 15 <= num <= 2 ** 15 - 1):
        to_add = 2 ** 16 if num < 0 else - 2 ** 16
        num += to_add
    return num

def _arithmetic_operation(operand, ram, registers, operation):
    """Standard arithmetic operations happen here"""
    acc_denary = bin_to_num(registers['ACC'], True)
    value = _get_val(operand, ram)
    result = operation(acc_denary, value)
    return num_to_bin(_overlap_accumulator(result), True, length=16)

RUN_TIME_METHOD = {}


def _add(operand, ram, registers):
    addition = lambda num1, num2: num1 + num2
    registers['ACC'] = _arithmetic_operation(operand, ram, registers, addition)

RUN_TIME_METHOD['ADD'] = _add


def _sub(operand, ram, registers):
    subtraction = lambda num1, num2: num1 - num2
    registers['ACC'] = _arithmetic_operation(operand, ram, registers, subtraction)

RUN_TIME_METHOD['SUB'] = _sub


def _not(operand, ram, registers):
    new_acc = ''
    for bit in registers['ACC']:
        new_acc += '0' if bit == '1' else '1'
    registers['ACC'] = new_acc

RUN_TIME_METHOD['NOT'] = _not


def _bor(operand, ram, registers):
    bitwise_or = lambda num1, num2: num1 | num2
    registers['ACC'] = _arithmetic_operation(operand, ram, registers, bitwise_or)


RUN_TIME_METHOD['BOR'] = _bor


def _and(operand, ram, registers):
    bitwise_and = lambda num1, num2: num1 & num2
    registers['ACC'] = _arithmetic_operation(operand, ram, registers, bitwise_and)

RUN_TIME_METHOD['AND'] = _and


def _xor(operand, ram, registers):
    bitwise_xor = lambda num1, num2: num1 ^ num2
    registers['ACC'] = _arithmetic_operation(operand, ram, registers, bitwise_xor)

RUN_TIME_METHOD['XOR'] = _xor


def _shift(acc, shift_val):
    if shift_val > 0:
        result = '0' * shift_val + acc
        return result[:16]
    result = acc + '0' * -shift_val
    return result[-16:]

def _lsh(operand, ram, registers):
    shift_val = _get_val(operand, ram) + 1
    registers['ACC'] = _shift(registers['ACC'], shift_val)

RUN_TIME_METHOD['LSH'] = _lsh


def _rsh(operand, ram, registers):
    shift_val = _get_val(operand, ram) + 1
    registers['ACC'] = _shift(registers['ACC'], -shift_val)

RUN_TIME_METHOD['RSH'] = _rsh


def _end(operand, ram, registers):
    registers['PC'] = len(ram)

RUN_TIME_METHOD['END'] = _end


def _bra(operand, ram, registers):
    registers['PC'] = bin_to_num(operand, False)

RUN_TIME_METHOD['BRA'] = _bra


def _brp(operand, ram, registers):
    if registers['ACC'][len(registers['ACC']) - 1] == '0':
        registers['PC'] = bin_to_num(operand, False)

RUN_TIME_METHOD['BRP'] = _brp


def _brz(operand, ram, registers):
    if registers['ACC'] == '0' * 16:
        registers['PC'] = bin_to_num(operand, False)

RUN_TIME_METHOD['BRZ'] = _brz


def _lda(operand, ram, registers):
    value = _get_val(operand, ram)
    registers['ACC'] = num_to_bin(value, True, length=16)

RUN_TIME_METHOD['LDA'] = _lda


def _sta(operand, ram, registers):
    ram_loc = bin_to_num(operand, False)
    if len(ram) - 1 < ram_loc:
        raise ValueError('Tried storing the accumulator\'s value at ram location #' + str(ram_loc) + ' when ram has #' + str(len(ram)) + ' locations.')
    ram[ram_loc] = registers['ACC']

RUN_TIME_METHOD['STA'] = _sta


def _inp(operand, ram, registers):
    if registers['input']:
        registers['ACC'] = registers['input']
    else:
        return {'input_needed': True}

RUN_TIME_METHOD['INP'] = _inp


def _out(operand, ram, registers):
    return {'output': str(bin_to_num(registers['ACC'], True))}

RUN_TIME_METHOD['OUT'] = _out
