from tkinter import *
from src.BinaryConversion import bin_to_num, num_to_bin
from src.Lexical import lexical_analysis, strip_comments, remove_white_space
from src.Syntactical import syntax_analysis
from src.LoadToRam import load_to_ram
from src.Execution import execute


def _display_registers(program_data, common_widgets):
    """Displays the current values of each register"""
    registers = program_data['registers']
    acc_value = bin_to_num(registers['ACC'], True)
    display_message = 'ACC: ' + str(acc_value) + '\nPC: ' + str(registers['PC'])
    common_widgets['registers_var'].set(display_message)

def reset_registers(program_data, common_widgets):
    """Resets the values of the registers to 0"""
    if program_data['input_needed']:
        return

    program_data['registers']['ACC'] = '0' * 16
    program_data['registers']['PC'] = 0
    _display_registers(program_data, common_widgets)


def _display_ram(program_data, common_widgets):
    """Goes through the ram and displays the contents with each memory address as 4 hexadecimal digits"""
    hex_ram = [
        str(hex(bin_to_num(address[::-1], False)))
        for address in program_data['ram']
    ]

    # Making all the hex addresses 4 digits long by having the 6 - len(old_hex)
    hex_display_format = lambda old_hex: old_hex[2:].upper() + '0' * (6 - len(old_hex))
    hex_ram = list(map(hex_display_format, hex_ram))

    common_widgets['ram_display_var'].set(' '.join(hex_ram))


def _create_src_lines(program_data, tokenised_src):
    # Turning a 2D array of all tokens into a 1D array of lines
    program_data['src_lines'] = [' '.join(line) for line in tokenised_src]


def _update_curr_line_output(program_data, common_widgets):
    """Updating the current line output text box"""
    # Defaulting as the end of the program
    line = 'END OF PROGRAM'

    # If it's not at the end of the program, it will use the current line instead
    line_index = program_data['registers']['PC']
    if line_index < len(program_data['src_lines']):
        line = program_data['src_lines'][line_index]

    # Applying the update to the text output widget
    common_widgets['curr_line_output'].config(state=NORMAL)
    common_widgets['curr_line_output'].delete(1.0, END)
    common_widgets['curr_line_output'].insert(END, line)
    common_widgets['curr_line_output'].config(state=DISABLED)


def _find_var_instructions(program_data, tokenised_src):
    var_instructions = []
    for index, line in enumerate(tokenised_src):
        if 'VAR' in line:
            entry = {'memory_address': index, 'name':line[0]}
            var_instructions.append(entry)
    program_data['var_instructions'] = var_instructions


def _display_variables(program_data, common_widgets):
    var_box = common_widgets['variable_output']

    # Deleting the contents of the variable output box
    var_box.delete(0, var_box.size())

    for entry in program_data['var_instructions']:
        output = entry['name'] + ': '

        ram_value = program_data['ram'][entry['memory_address']]
        output += str(bin_to_num(ram_value, True))

        var_box.insert(END, output)


def _protected_translate(source_code, common_widgets):
    try:
        id_list = lexical_analysis(source_code)
        full_type_list = syntax_analysis(id_list)
        return load_to_ram(full_type_list)
    except Exception as err:
        common_widgets['output_box'].insert(END, err)

def generate_ram(program_data, common_widgets):
    """Generating ram from the text in the code input box"""
    source_code = common_widgets['code_input'].get('1.0', END)
    ram = _protected_translate(source_code, common_widgets)
    if not ram:
        return
    program_data['ram'] = ram

    tokenised_src = remove_white_space(strip_comments(source_code))

    _create_src_lines(program_data, tokenised_src)
    _update_curr_line_output(program_data, common_widgets)

    _find_var_instructions(program_data, tokenised_src)
    _display_variables(program_data, common_widgets)

    reset_registers(program_data, common_widgets)
    _display_ram(program_data, common_widgets)


def _protected_execute(program_data, common_widgets, step):
    try:
        return execute(program_data['ram'], registers=program_data['registers'], step=step)
    except Exception as err:
        common_widgets['output_box'].insert(END, err)

def _execute_ram(program_data, common_widgets, step):
    """Executes the ram with the current registers values"""
    # Making sure the input isn't needed before execution
    if program_data['input_needed']:
        return

    program_output = _protected_execute(program_data, common_widgets, step)
    if not program_output:
        return

    # Displaying the program's output
    if program_output['output']:
        for line in program_output['output']:
            common_widgets['output_box'].insert(END, line)

    # If the last instruction was the INP instruction, it will require input
    if program_output['input_needed']:
        common_widgets['input_field'].config(state=NORMAL)
        common_widgets['execute_once'].config(state=DISABLED)
        common_widgets['execute_prog'].config(state=DISABLED)
        program_data['input_needed'] = True

    _update_curr_line_output(program_data, common_widgets)
    _display_variables(program_data, common_widgets)
    _display_registers(program_data, common_widgets)
    _display_ram(program_data, common_widgets)


def rest_of_program(program_data, common_widgets):
    """Executes the remaining lines of the program"""
    program_data['step'] = False
    _execute_ram(program_data, common_widgets, step=False)

def step_once(program_data, common_widgets):
    """Executes the next instruction in ram"""
    program_data['step'] = True
    _execute_ram(program_data, common_widgets, step=True)


def input_field_validate(inp_field, common_widgets):
    """Validates the input field and makes sure it's acceptable"""
    next_state = inp_field.get()
    try:
        # Checks if the next_state is a '-' or if it can be cast as an integer
        if next_state and next_state != '-':
            int(next_state)

        # Sets the old string to the current string
        common_widgets['input_text_old'] = next_state
    except ValueError:
        # If it can't be cast as an integer, it will be reset to the old string
        inp_field.set(common_widgets['input_text_old'])


def _check_acc_compatible(value):
    """More validation on the input field to check that the accumulator will be able to handle it"""
    # Tries casting the value as an integer and also checking if it's within a range
    try:
        if -2**15 <= int(value) < 2**15:
            return True
        else:
            return False
    except ValueError:
        return False

def input_field_button(program_data, common_widgets):
    """"When the user clicks the submit button, this function will be called"""
    if not program_data['input_needed']:
        return

    input_value = common_widgets['input_text']

    if _check_acc_compatible(input_value.get()):
        common_widgets['input_field'].config(state=DISABLED)
        common_widgets['execute_once'].config(state=NORMAL)
        common_widgets['execute_prog'].config(state=NORMAL)

        # Making the input register the binary version of the input value
        program_data['registers']['input'] = num_to_bin(int(input_value.get()), True, length=16)
        program_data['input_needed'] = False

        # Resetting the input field data
        input_value.set('')
        common_widgets['input_text_old'] = ''

        _execute_ram(program_data, common_widgets, step=program_data['step'])

    else:
        print('Value out of range! please choose a value within the range: ' + str(-2**15) + ' -> ' + str(2 ** 15 - 1))
