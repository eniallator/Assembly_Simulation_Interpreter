"""Here i implement lexical analysis for the project"""
import re
from src.SymbolTable import SymbolTable
from src.Instructions.Data import INSTRUCTION_DATA


def strip_comments(file_contents):
    """Strips comments from the file using regex i have figured out"""
    without_comments = re.sub(r'\#(.*)?', '', file_contents)
    return without_comments


def remove_white_space(no_comments):
    """Splits tokens up and removes whitespace. Also gets rid of empty tokens"""
    tokens = []
    # Iterating over each line
    for line in no_comments.split('\n'):
        # Splitting the line at the white space
        line_instances = re.split(r'\s', line)
        # Removing any of the splits that are empty
        line_tokens = [token for token in line_instances if token]

        # Only add the new line token if there is anything there
        if line_tokens:
            tokens.append(line_tokens)
    return tokens


def _gen_constant_id(self, in_string):
    """Gets the constant's id"""
    return 'c' + in_string[1:]

def _gen_instruction_id(self, in_string):
    """Gets the array index of the instruction from INSTRUCTION_DATA"""
    for index, instruction in enumerate(INSTRUCTION_DATA):
        if instruction['name'] == in_string:
            return 'i' + str(index)

def _gen_address_id(self, in_string):
    """Gets the memory address id"""
    return 'a' + in_string

def _gen_variable_id(self, in_string):
    """Gets what the current counter is up to"""
    unique_id = 'v' + str(self['counter'])
    self['counter'] += 1
    return unique_id

def _get_types():
    """Returns all token types"""
    return [
        # Any constants will be a 'c'
        {
            # All check_in_type keys will return true if it's the correct type
            'check_in_type': lambda token: re.search(r'^_-?\d+$', token),
            'gen_id': _gen_constant_id
        },
        # Any instructions will be an 'i'
        {
            'check_in_type': lambda token: token in [instruction['name'] for instruction in INSTRUCTION_DATA],
            'gen_id': _gen_instruction_id
        },
        # Any memory addresses will be an 'a'
        {
            'check_in_type': lambda token: re.search(r'^\d+$', token),
            'gen_id': _gen_address_id
        },
        # Any variables/loop start and end points will be a 'v'
        {
            'check_in_type': lambda token: re.search(r'^[a-zA-Z][a-zA-Z0-9_]*$', token),
            'gen_id': _gen_variable_id,
            'counter': 0
        }
    ]

def create_ids(lines):
    """Makes another 2D array of tokens replaced by their ids"""
    id_types = _get_types()
    # Creating the base node of the symbol table binary tree
    new_symbol_table = SymbolTable()
    # Declaring a new_lines variable to then return
    new_lines = []

    for line in lines:
        curr_line = []
        for token in line:
            for curr_type in id_types:
                # Checking if the regex format matches the current token
                if curr_type['check_in_type'](token):
                    # Assigning existing_entry if the token is a dupelicate, otherwise it will be None
                    existing_entry = new_symbol_table.find(token)

                    # Adding the existing_entry's id to the line if it isn't None
                    if existing_entry:
                        curr_line.append(existing_entry.unique_id)
                    
                    # Otherwise, creating the entry and then adding the id to the line
                    else:
                        curr_unique_id = curr_type['gen_id'](curr_type, token)
                        new_symbol_table.create(token, curr_unique_id)
                        curr_line.append(curr_unique_id)
                    break
            
            # If the for loop doesn't break, it will raise an error meaning it hasn't been able to give it a type
            else:
                raise ValueError('Unexpected token: "' + token + '"')
        # Adding the current line to the rest
        new_lines.append(curr_line)
    return new_lines


def lexical_analysis(file_contents):
    """Putting all lexical analysis aspects together here"""
    comments_stripped = strip_comments(file_contents)
    tokenised = remove_white_space(comments_stripped)
    id_lines = create_ids(tokenised)
    return id_lines
