"""Syntactical analysis for the assembly language"""
from src.Instructions.Data import INSTRUCTION_DATA, INSTRUCTION_NAME_IDS


def _change_to_branch(branch_id, id_list):
    """Changes all instances of the branch id to start with 'b' instead of 'v'"""
    for line in id_list:
        for i, token_id in enumerate(line):
            if token_id == branch_id:
                line[i] = 'b' + token_id[1:]

def find_branch_types(id_list):
    """Find all the branch ids and change their prefix to start with b"""
    # Copying the id_list
    full_type_list = list(id_list)

    # Getting the branch instructions ids that would match the full_type_list's values
    branch_indexes = [
        'i' + str(index) for index, instruction in enumerate(INSTRUCTION_NAME_IDS) 
        if instruction.startswith('BR')
    ]

    for line in full_type_list:
        for token_index, token_id in enumerate(line):
            if token_id in branch_indexes and token_index + 1 < len(line) and line[token_index + 1].startswith('v'):
                branch_id = line[token_index + 1]
                _change_to_branch(branch_id, full_type_list)
    
    return full_type_list


def check_syntax(id_list):
    """Main checker for syntax structure and types"""
    for line_number, line in enumerate(id_list):
        if len(line) > 3:
            raise SyntaxError('Too many tokens on line #' + str(line_number))
        
        for index, token_id in enumerate(line):
            if token_id.startswith('i'):
                # Checking if there are 2 operands to either side
                if not index and len(line) > 2 or index > 1:
                    raise SyntaxError('Too many tokens on line #' + str(line_number))

                instruction_index = int(token_id[1:])
                curr_syntax = INSTRUCTION_DATA[instruction_index]['syntax']

                # This checks if the type to whatever side of the instruction is accepted or not
                check_matches_type = lambda side, index: side in curr_syntax and line[index][0] in curr_syntax[side]['type']
                
                # If there isn't a type to whatever side of the instruction, this checks if that side is optional or if it even takes a type on that side
                check_not_there = lambda side, index_condition: not index_condition and (side not in curr_syntax or curr_syntax[side]['optional'])

                # Checking the left side of the instruction
                if not (check_matches_type('left', 0) or check_not_there('left', index)):
                    raise SyntaxError('Misused left side of instruction on line #' + str(line_number))
                
                # Checking the right side of the instruction
                if not (check_matches_type('right', len(line) - 1) or check_not_there('right', line[len(line) - 1][0] != 'i')):
                    raise SyntaxError('Misused right side of instruction on line #' + str(line_number))
                break
        else:
            raise SyntaxError('No instruction found on line #' + str(line_number))


def syntax_analysis(id_list):
    """Putting all syntactical analysis aspects together here"""
    full_type_list = find_branch_types(id_list)
    check_syntax(full_type_list)
    return full_type_list
