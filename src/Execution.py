"""The execution stage for this project"""
from src.Instructions.RunTimeMethods import RUN_TIME_METHOD
from src.Instructions.Data import INSTRUCTION_NAME_IDS
from src.BinaryConversion import bin_to_num

def execute(ram, registers=None, step=False):
    """The execution stage of the program"""
    # Defining the registers to be used upon execution
    if not registers:
        registers = {
            'ACC': '0' * 16,
            'PC': 0,
            'input': None
        }
    
    data = {
        'output': [],
        'input_needed': False
    }

    while registers['PC'] < len(ram):
        old_pc = registers['PC']

        # Grabbing the operator and operand from the current memory address
        operator = ram[registers['PC']][:4]
        operand = ram[registers['PC']][4:]

        # Executing the operator/operand pair
        curr_instruction = INSTRUCTION_NAME_IDS[bin_to_num(operator, False)]
        instruction_output = RUN_TIME_METHOD[curr_instruction](operand, ram, registers)
        registers['input'] = None

        # Adding the instruction's output to the program output
        if instruction_output:
            if 'output' in instruction_output:
                data['output'].append(instruction_output['output'])

            elif 'input_needed' in instruction_output and instruction_output['input_needed']:
                data['input_needed'] = True
                return data

        # Incrementing the PC if the PC has not changed upon instruction execution (Preventing branch instructions from going to the wrong line)
        if registers['PC'] == old_pc:
            registers['PC'] += 1
        
        # Breaking out of the loop if step is true
        if step:
            break
    
    return data
