"""Loads the id_list contents into their ram equivalent values"""
from src.Instructions.Data import INSTRUCTION_DATA, INSTRUCTION_NAME_IDS

def _generate_var_ref(id_list, var_id):
    """Generates a key/value dictionary for quickly getting VAR locations when loading into ram"""
    var_ref = {}
    for line_num, line in enumerate(id_list):
        # Tries to index the var_id and if it can't, it will go to the except statement
        try:
            line.index(var_id)
            var_ref[line[0]] = line_num
        except ValueError:
            pass
    return var_ref

def _generate_branch_ref(id_list):
    """Generates a dictionary of branch end points"""
    branch_ref = {}

    # Iterating over the id_list backwards
    # Just since i want the branches to always point towards the first instance of them
    for line_index in range(len(id_list) - 1, -1, -1):
        if id_list[line_index][0].startswith('b'):
            branch_ref[id_list[line_index][0]] = line_index
    return branch_ref

def load_to_ram(id_list):
    """Loads the program into a simulated RAM"""
    # Creating the id of the VAR instruction
    var_id = 'i' + str(INSTRUCTION_NAME_IDS.index('VAR'))
    ram = [False] * len(id_list)
    var_ref = _generate_var_ref(id_list, var_id)
    branch_ref =_generate_branch_ref(id_list)

    for ram_loc, line in enumerate(id_list):
        for index, token_id in enumerate(line):
            # Finding the instruction on the line
            if token_id.startswith('i'):
                # Getting the reference to the current instruction for easy access
                curr_instruction = INSTRUCTION_DATA[int(token_id[1:])]

                # Getting the operator and operand
                operator = token_id
                operand = line[index + 1] if len(line) > index + 1 else None

                # Creating the ram location
                ram[ram_loc] = curr_instruction['gen_ram'](operator, operand, var_ref, branch_ref)
    return ram
