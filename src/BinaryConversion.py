"""Utility functions for converting between base 10 and binary"""

def _twos_compliment(bin_num):
    flipped_bits = ['0' if curr_bit == '1' else '1' for curr_bit in bin_num]
    index = 0

    # While loop for adding 1 to the binary number
    while len(flipped_bits) > index:
        if flipped_bits[index] == '0':
            flipped_bits[index] = '1'
            break
        else:
            flipped_bits[index] = '0'
            index += 1
    return ''.join(flipped_bits)

def num_to_bin(num, twos_compliment, length=False):
    """Converts from denary to binary and by default uses twos_compliment"""
    # Converting the number to binary and making it a string
    bin_num = str(bin(num))

    if num < 0 and not twos_compliment:
        raise ValueError('Unexpected negative number: ' + str(num))
    
    # If the number is negative and 2**(length - 1) is -num, then it will return an output straight away - to get around an error
    if - num == 2 ** (length - 1):
        return '0' * (length - 1) + '1'
    # If the number is a negative number, it will be converted to twos_compliment
    elif bin_num.startswith('-'):
        # Grabbing just the binary digits and then reversing them since it's easier to use. Then adding '1' since it's twos compliment
        bin_num = _twos_compliment(bin_num[3:][::-1]) + '1'
    else:
        # Same kinda thing but this time checking if the extra digit is necessary or not
        bin_num = bin_num[2:][::-1] + ('0' if twos_compliment else '')
    
    if type(length) is not int or len(bin_num) == length:
        return bin_num

    elif len(bin_num) < length:
        # Making sure the number of digits fit within the given length
        tail_value = '1' if num < 0 else '0'
        tail = tail_value * (length - len(bin_num))
        return bin_num + tail
    
    # If the binary is larger than the given length parameter
    else:
        # Calculating the range of accepted values
        accepted_range = []
        if twos_compliment:
            accepted_range.append(- 2 ** (length - 1))
            accepted_range.append(2 ** (length - 1) - 1)
        else:
            accepted_range.append(0)
            accepted_range.append(2 ** (length) - 1)
        raise ValueError('Too large a number. Given number: ' + str(num) + ' possible range of values: ' + str(accepted_range[0]) + ' -> ' + str(accepted_range[1]))

def bin_to_num(bin_num, twos_compliment):
    """Converts a string of 1s and 0s to their denary equivalent"""
    denary_num = 0
    for exponent, bit in enumerate(bin_num):
        if bit == '1':
            # Checks if it needs to minus the last digit if it's twos_compliment
            if twos_compliment and exponent == len(bin_num) - 1:
                denary_num -= 2 ** exponent
            else:
                denary_num += 2 ** exponent
    return denary_num
