"""A unit testing script that will test Lexical, Syntactical and LoadToRam script files in src using predefined UATs"""

import unittest
import src.Lexical as lexical
import src.Syntactical as syntactical
from src.LoadToRam import load_to_ram
from Tests.UATUnitTestData import UAT_TESTS

class TestSystemUAT(unittest.TestCase):
    """Testing that the UAT give expected outputs at each stage"""

    def test_first_lexical(self):
        """Testing that the first UAT gives the expected output from lexical analysis"""
        test_data = UAT_TESTS[0]['src_program']
        expected = UAT_TESTS[0]['lexical_output']
        result = lexical.lexical_analysis(test_data)
        self.assertEqual(expected, result)

    def test_first_syntactical(self):
        """Testing that the first UAT gives the expected output from syntactical analysis"""
        test_data = UAT_TESTS[0]['lexical_output']
        expected = UAT_TESTS[0]['syntactical_output']
        result = syntactical.syntax_analysis(test_data)
        self.assertEqual(expected, result)

    def test_first_load_to_ram(self):
        """Testing that the first UAT gives the expected output from loading to ram"""
        test_data = UAT_TESTS[0]['syntactical_output']
        expected = UAT_TESTS[0]['load_to_ram_output']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_second_lexical(self):
        """Testing that the second UAT gives the expected output from lexical analysis"""
        test_data = UAT_TESTS[1]['src_program']
        expected = UAT_TESTS[1]['lexical_output']
        result = lexical.lexical_analysis(test_data)
        self.assertEqual(expected, result)

    def test_second_syntactical(self):
        """Testing that the second UAT gives the expected output from syntactical analysis"""
        test_data = UAT_TESTS[1]['lexical_output']
        expected = UAT_TESTS[1]['syntactical_output']
        result = syntactical.syntax_analysis(test_data)
        self.assertEqual(expected, result)

    def test_second_load_to_ram(self):
        """Testing that the second UAT gives the expected output from loading to ram"""
        test_data = UAT_TESTS[1]['syntactical_output']
        expected = UAT_TESTS[1]['load_to_ram_output']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_third_lexical(self):
        """Testing that the third UAT gives the expected output from lexical analysis"""
        test_data = UAT_TESTS[2]['src_program']
        expected = UAT_TESTS[2]['lexical_output']
        result = lexical.lexical_analysis(test_data)
        self.assertEqual(expected, result)

    def test_third_syntactical(self):
        """Testing that the third UAT gives the expected output from syntactical analysis"""
        test_data = UAT_TESTS[2]['lexical_output']
        expected = UAT_TESTS[2]['syntactical_output']
        result = syntactical.syntax_analysis(test_data)
        self.assertEqual(expected, result)

    def test_third_load_to_ram(self):
        """Testing that the third UAT gives the expected output from loading to ram"""
        test_data = UAT_TESTS[2]['syntactical_output']
        expected = UAT_TESTS[2]['load_to_ram_output']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)
