"""A unit testing script that will test the RunTimeMethods script file in src/Instructions"""

import unittest
from src.Instructions.RunTimeMethods import RUN_TIME_METHOD as run_time_methods

def get_error_message(func, err_type):
    """Gets the error message that a function gives"""
    try:
        func()
    except err_type as err:
        return str(err)
    return ''

class TestRunTimeMethods(unittest.TestCase):
    """Testing that all the run time methods work as expected"""

    def setUp(self):
        """Done before each of the below unit tests"""
        self.test_ram = ['1' + '0' * 15, '01' + '0' * 14, '11' + '0' * 14]
        self.test_registers = {
            'ACC': '1' + '0' * 15,
            'PC': 0,
            'input': None
        }
        self.test_data = ['0' * 12, self.test_ram, self.test_registers]

    def test_add_positive(self):
        """Checks that the add run time method gives the correct output with a positive input"""
        self.test_data[0] = '001010000000'
        expected = '1101' + '0' * 12
        run_time_methods['ADD'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_add_negative(self):
        """Checks that the add run time method gives the correct output with a negative input"""
        self.test_registers['ACC'] = '0111111111111111'
        self.test_data[0] = '011111111111'
        expected = '10' + '1' * 14
        run_time_methods['ADD'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_add_positive_overlap(self):
        """Checks that the add run time method gives the correct output when overlapping from positive to negative"""
        self.test_registers['ACC'] = '1' * 15 + '0'
        self.test_data[0] = '010000000000'
        expected = '0' * 15 + '1'
        run_time_methods['ADD'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_add_negative_overlap(self):
        """Checks that the add run time method gives the correct output when overlapping from negative to positive"""
        self.test_registers['ACC'] = '0' * 15 + '1'
        self.test_data[0] = '011111111111'
        expected = '1' * 15 + '0'
        run_time_methods['ADD'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_sub_positive(self):
        """Checks that the sub run time method gives the correct output with a positive input"""
        self.test_data[0] = '010000000000'
        expected = '0' * 16
        run_time_methods['SUB'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_sub_negative(self):
        """Checks that the sub run time method gives the correct output with a negative input"""
        self.test_registers['ACC'] = '10' + '1' * 14
        self.test_data[0] = '001111111111'
        expected = '1' * 16
        run_time_methods['SUB'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_not(self):
        """Checks that the not run time method gives the correct output with a valid input"""
        expected = '0' + '1' * 15
        run_time_methods['NOT'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_bor(self):
        """Checks that the bor run time method gives the correct output with a valid input"""
        self.test_data[0] = '011000000000'
        expected = '11' + '0' * 14
        run_time_methods['BOR'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_and(self):
        """Checks that the and run time method gives the correct output with a valid input"""
        self.test_data[0] = '011000000000'
        expected = '1' + '0' * 15
        run_time_methods['AND'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_xor(self):
        """Checks that the xor run time method gives the correct output with a valid input"""
        self.test_data[0] = '011000000000'
        expected = '01' + '0' * 14
        run_time_methods['XOR'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_lsh(self):
        """Checks that the lsh run time method gives the correct output with a valid input"""
        expected = '01' + '0' * 14
        run_time_methods['LSH'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_rsh(self):
        """Checks that the rsh run time method gives the correct output with a valid input"""
        expected = '0' * 16
        run_time_methods['RSH'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_end(self):
        """Checks that the end run time method gives the correct output with a valid input"""
        expected = 3
        run_time_methods['END'](*self.test_data)
        result = self.test_registers['PC']
        self.assertEqual(expected, result)

    def test_bra(self):
        """Checks that the bra run time method gives the correct output with a valid input"""
        self.test_data[0] = '100000000000'
        expected = 1
        run_time_methods['BRA'](*self.test_data)
        result = self.test_registers['PC']
        self.assertEqual(expected, result)

    def test_brp_positive(self):
        """Checks that the brp run time method gives the correct output with a positive input"""
        self.test_data[0] = '100000000000'
        expected = 1
        run_time_methods['BRP'](*self.test_data)
        result = self.test_registers['PC']
        self.assertEqual(expected, result)

    def test_brp_negative(self):
        """Checks that the brp run time method gives the correct output with a negative input"""
        self.test_registers['ACC'] = '0' * 15 + '1'
        self.test_data[0] = '100000000000'
        expected = 0
        run_time_methods['BRP'](*self.test_data)
        result = self.test_registers['PC']
        self.assertEqual(expected, result)

    def test_brz_zero(self):
        """Checks that the brz run time method gives the correct output with zero as the input"""
        self.test_registers['ACC'] = '0' * 16
        self.test_data[0] = '100000000000'
        expected = 1
        run_time_methods['BRZ'](*self.test_data)
        result = self.test_registers['PC']
        self.assertEqual(expected, result)

    def test_brz_not_zero(self):
        """Checks that the brz run time method gives the correct output with a non-zero input"""
        self.test_data[0] = '100000000000'
        expected = 0
        run_time_methods['BRZ'](*self.test_data)
        result = self.test_registers['PC']
        self.assertEqual(expected, result)

    def test_lda(self):
        """Checks that the lda run time method gives the correct output with a valid input"""
        self.test_data[0] = '010110000000'
        expected = '1011000000000000'
        run_time_methods['LDA'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_sta_valid(self):
        """Checks that the sta run time method gives the correct output with a valid input"""
        self.test_data[0] = '100000000000'
        expected = '1' + '0' * 15
        run_time_methods['STA'](*self.test_data)
        result = self.test_ram[1]
        self.assertEqual(expected, result)

    def test_sta_invalid(self):
        """Checks that the sta run time method errors when given an invalid input"""
        self.test_data[0] = '000000000001'
        expected = 'Tried storing the accumulator\'s value at ram location #2048 when ram has #3 locations.'
        result = get_error_message(lambda: run_time_methods['STA'](*self.test_data), ValueError)
        self.assertEqual(expected, result)

    def test_inp_with_input(self):
        """Checks that the inp run time method gives the correct output with input"""
        self.test_registers['input'] = '1101000000000000'
        expected = self.test_registers['input']
        run_time_methods['INP'](*self.test_data)
        result = self.test_registers['ACC']
        self.assertEqual(expected, result)

    def test_inp_without_input(self):
        """Checks that the inp run time method gives the correct output without input"""
        expected = {'input_needed': True}
        result = run_time_methods['INP'](*self.test_data)
        self.assertEqual(expected, result)

    def test_out(self):
        """Checks that the out run time method will return the correct output when called"""
        self.test_registers['ACC'] = '1101' + '0' * 14
        expected = {'output': '11'}
        result = run_time_methods['OUT'](*self.test_data)
        self.assertEqual(expected, result)
