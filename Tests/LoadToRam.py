"""A unit testing script that will test the LoadToRam script file in src"""

import unittest
from src.LoadToRam import load_to_ram

class TestLoadToRam(unittest.TestCase):
    """Testing that loading to ram works as expected"""

    def test_add(self):
        """Checks that the add instruction loads as expected"""
        test_data = [['i0', 'c11']]
        expected = ['0000011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_sub(self):
        """Checks that the sub instruction loads as expected"""
        test_data = [['i1', 'c11']]
        expected = ['1000011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_not(self):
        """Checks that the not instruction loads as expected"""
        test_data = [['i2']]
        expected = ['0100000000000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_bor(self):
        """Checks that the bor instruction loads as expected"""
        test_data = [['i3', 'c11']]
        expected = ['1100011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_and(self):
        """Checks that the and instruction loads as expected"""
        test_data = [['i4', 'c11']]
        expected = ['0010011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_xor(self):
        """Checks that the xor instruction loads as expected"""
        test_data = [['i5', 'c11']]
        expected = ['1010011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_lsh(self):
        """Checks that the lsh instruction loads as expected"""
        test_data = [['i6', 'c11']]
        expected = ['0110011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_rsh(self):
        """Checks that the rsh instruction loads as expected"""
        test_data = [['i7', 'c11']]
        expected = ['1110011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_end(self):
        """Checks that the end instruction loads as expected"""
        test_data = [['i8']]
        expected = ['0001000000000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_bra(self):
        """Checks that the bra instruction loads as expected"""
        test_data = [['i9', 'a12']]
        expected = ['1001110100000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_brp(self):
        """Checks that the brp instruction loads as expected"""
        test_data = [['i10', 'a12']]
        expected = ['0101110100000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_brz(self):
        """Checks that the brz instruction loads as expected"""
        test_data = [['i11', 'a12']]
        expected = ['1101110100000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_lda(self):
        """Checks that the lda instruction loads as expected"""
        test_data = [['i12', 'c11']]
        expected = ['0011011010000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_sta(self):
        """Checks that the sta instruction loads as expected"""
        test_data = [['i13', 'a12']]
        expected = ['1011110100000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_inp(self):
        """Checks that the inp instruction loads as expected"""
        test_data = [['i14']]
        expected = ['0111000000000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)

    def test_out(self):
        """Checks that the out instruction loads as expected"""
        test_data = [['i15']]
        expected = ['1111000000000000']
        result = load_to_ram(test_data)
        self.assertEqual(expected, result)
