"""A unit testing script that will test the Lexical script file in src"""

import unittest
import src.Lexical as lexical

def get_error_message(func, err_type):
    """Gets the error message that a function gives"""
    try:
        func()
    except err_type as err:
        return str(err)
    return ''

class TestLexical(unittest.TestCase):
    """Testing that lexical analysis works as expected"""

    def test_strip_comments_with_comments(self):
        """Testing the strip comments function with test data that has a comment"""
        test_data = 'this\nis\na\n#comment'
        expected = 'this\nis\na\n'
        result = lexical.strip_comments(test_data)
        self.assertEqual(expected, result)

    def test_strip_comments_without_comment(self):
        """Testing the strip comments function with test data that has no comments"""
        test_data = 'this\nis\nnot\na\ncomment'
        expected = test_data
        result = lexical.strip_comments(test_data)
        self.assertEqual(expected, result)

    def test_remove_white_space_spaces_newlines(self):
        """Testing the remove white space function with test data that has both spaces and newlines"""
        test_data = 'there are both\n\nspaces and newlines\nhere'
        expected = [['there', 'are', 'both'], ['spaces', 'and', 'newlines'], ['here']]
        result = lexical.remove_white_space(test_data)
        self.assertEqual(expected, result)

    def test_remove_white_space_just_spaces_newlines(self):
        """Testing the remove white space function with test data that is only spaces and newlines"""
        test_data = ' \n \n'
        expected = []
        result = lexical.remove_white_space(test_data)
        self.assertEqual(expected, result)

    def test_create_ids_valid(self):
        """Testing the create ids function with test data that has 2 of each data type and shouldn't error"""
        test_data = [['ADD', 'SUB', '_-100', '_902', '36', '12', 'apples', 'oranges']]
        expected = [['i0', 'i1', 'c-100', 'c902', 'a36', 'a12', 'v0', 'v1']]
        result = lexical.create_ids(test_data)
        self.assertEqual(expected, result)

    def test_create_ids_invalid(self):
        """Testing the create ids function with test data that will cause an error"""
        test_data = [['12num']]
        expected = 'Unexpected token: "12num"'
        result = get_error_message(lambda: lexical.create_ids(test_data), ValueError)
        self.assertEqual(expected, result)
