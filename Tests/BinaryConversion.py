"""A unit testing script that will test the BinaryConversion script file in src"""

import unittest
import src.BinaryConversion as binary_conversion


def get_error_message(func, err_type):
    """Gets the error message that a function gives"""
    try:
        func()
    except err_type as err:
        return str(err)
    return ''


class TestBinaryConversion(unittest.TestCase):
    """Testing that the binary conversion works as expected"""

    def test_num_to_bin_negative(self):
        """Checks that num_to_bin errors when given a negative number without twos compliment"""
        test_data = -1
        expected = 'Unexpected negative number: -1'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, False), ValueError)
        self.assertEqual(expected, result)

    def test_bin_to_num(self):
        """Checks that bin_to_num returns the correct value when given a valid input"""
        test_data = '0011'
        expected = 12
        result = binary_conversion.bin_to_num(test_data, False)
        self.assertEqual(expected, result)

    def test_bin_to_num_positive_twos_compliment(self):
        """Checks that bin_to_num returns the correct output when given a twos compliment input"""
        test_data = '00110'
        expected = 12
        result = binary_conversion.bin_to_num(test_data, True)
        self.assertEqual(expected, result)

    def test_bin_to_num_negative_twos_compliment(self):
        """Checks that bin_to_num returns a negative value when doing twos compliment"""
        test_data = '00101'
        expected = -12
        result = binary_conversion.bin_to_num(test_data, True)
        self.assertEqual(expected, result)

    # Positive non twos compliment
    def test_num_to_bin_valid(self):
        """Checks that num_to_bin returns the correct value when given a valid input"""
        test_data = 67
        expected = '1100001'
        result = binary_conversion.num_to_bin(test_data, False, length=7)
        self.assertEqual(expected, result)

    def test_num_to_bin_valid_extreme(self):
        """Checks that num_to_bin returns the correct value when given a valid extreme input"""
        test_data = 15
        expected = '1111'
        result = binary_conversion.num_to_bin(test_data, False, length=4)
        self.assertEqual(expected, result)

    def test_num_to_bin_invalid_extreme(self):
        """Checks that num_to_bin errors when given an invalid extreme input"""
        test_data = 16
        expected = 'Too large a number. Given number: 16 possible range of values: 0 -> 15'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, False, length=4), ValueError)
        self.assertEqual(expected, result)

    def test_num_to_bin_invalid(self):
        """Checks that num_to_bin errors when given an invalid input"""
        test_data = 9001
        expected = 'Too large a number. Given number: 9001 possible range of values: 0 -> 255'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, False, length=8), ValueError)
        self.assertEqual(expected, result)

    # Positive twos compliment
    def test_num_to_bin_twos_compliment_valid(self):
        """Checks that num_to_bin returns the correct value when given a valid input with twos compliment"""
        test_data = 67
        expected = '11000010'
        result = binary_conversion.num_to_bin(test_data, True, length=8)
        self.assertEqual(expected, result)

    def test_num_to_bin_twos_compliment_valid_extreme(self):
        """Checks that num_to_bin returns the correct value when given a valid extreme input with twos compliment"""
        test_data = 7
        expected = '1110'
        result = binary_conversion.num_to_bin(test_data, True, length=4)
        self.assertEqual(expected, result)

    def test_num_to_bin_twos_compliment_invalid_extreme(self):
        """Checks that num_to_bin errors when given an invalid extreme input with twos compliment"""
        test_data = 8
        expected = 'Too large a number. Given number: 8 possible range of values: -8 -> 7'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, True, length=4), ValueError)
        self.assertEqual(expected, result)

    def test_num_to_bin_twos_compliment_invalid(self):
        """Checks that num_to_bin errors when given an invalid input with twos compliment"""
        test_data = 9001
        expected = 'Too large a number. Given number: 9001 possible range of values: -128 -> 127'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, True, length=8), ValueError)
        self.assertEqual(expected, result)

    # Negative twos compliment
    def test_num_to_bin_negative_twos_compliment_valid(self):
        """Checks that num_to_bin returns the correct value when given a negative valid input"""
        test_data = -12
        expected = '00101'
        result = binary_conversion.num_to_bin(test_data, True, length=5)
        self.assertEqual(expected, result)

    def test_num_to_bin_negative_twos_compliment_valid_extreme(self):
        """Checks that num_to_bin returns the correct value when given a negative valid extreme input"""
        test_data = -8
        expected = '0001'
        result = binary_conversion.num_to_bin(test_data, True, length=4)
        self.assertEqual(expected, result)

    def test_num_to_bin_negative_twos_compliment_invalid_extreme(self):
        """Checks that num_to_bin errors when given a negative invalid extreme input"""
        test_data = -9
        expected = 'Too large a number. Given number: -9 possible range of values: -8 -> 7'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, True, length=4), ValueError)
        self.assertEqual(expected, result)

    def test_num_to_bin_negative_twos_compliment_invalid(self):
        """Checks that num_to_bin errors when given a negative invalid input"""
        test_data = -9001
        expected = 'Too large a number. Given number: -9001 possible range of values: -128 -> 127'
        result = get_error_message(lambda: binary_conversion.num_to_bin(test_data, True, length=8), ValueError)
        self.assertEqual(expected, result)
