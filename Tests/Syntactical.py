"""A unit testing script that will test the Syntactical script file in src"""

import unittest
import src.Syntactical as syntactical

def get_error_message(func, err_type):
    """Gets the error message that a function gives"""
    try:
        func()
    except err_type as err:
        return str(err)
    return ''


class TestSyntactical(unittest.TestCase):
    """Testing that syntactical analysis works as expected"""

    # Branch instruction ids: 9, 10, 11
    def test_find_branch_types(self):
        """Checks that the find_branch_types function works as expected"""
        test_data = [['i8', 'v0'], ['i9', 'v1'], ['i10', 'v2'], ['i11', 'v3'], ['i12', 'v4']]
        expected = [['i8', 'v0'], ['i9', 'b1'], ['i10', 'b2'], ['i11', 'b3'], ['i12', 'v4']]
        result = syntactical.find_branch_types(test_data)
        self.assertEqual(expected, result)

    def test_check_syntax_too_many_tokens(self):
        """Checks that the check_syntax errors if there are too many tokens on a line"""
        test_data = [['b0', 'i0', 'v1', 'SHOULD ERROR HERE']]
        expected = 'Too many tokens on line #0'
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)

    def test_check_syntax_too_many_tokens_left(self):
        """Checks that the check_syntax errors if there are too many tokens to the left of an instruction"""
        test_data = [['v0', 'v1', 'i0']]
        expected = 'Too many tokens on line #0'
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)

    def test_check_syntax_too_many_tokens_right(self):
        """Checks that the check_syntax errors if there are too many tokens to the right of an instruction"""
        test_data = [['i0', 'v0', 'v1']]
        expected = 'Too many tokens on line #0'
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)

    def test_check_syntax_no_instruction(self):
        """Checks that the check_syntax errors if there's no instruction on a line"""
        test_data = [['v0', 'v1', 'v2']]
        expected = 'No instruction found on line #0'
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)

    def test_check_syntax_optional(self):
        """Checks that the check_syntax errors if there's nothing to the right when it's not optional"""
        test_data = [['i0']]
        expected = 'Misused right side of instruction on line #0'
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)

    def test_check_syntax_wrong_type(self):
        """Checks that the check_syntax errors if the wrong data type has been given to the left of it"""
        test_data = [['c0', 'i0']]
        expected = 'Misused left side of instruction on line #0'
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)
    
    def test_check_syntax_valid(self):
        """Checks that the check_syntax does not error if a valid program has been passed to it"""
        test_data = [['i0', 'c100']]
        expected = ''
        result = get_error_message(lambda: syntactical.check_syntax(test_data), SyntaxError)
        self.assertEqual(expected, result)
