"""The main unit testing file where all unit tests will be executed and the output given"""

import unittest

# The method of executing unit tests from other locations
from Tests.Lexical import TestLexical
from Tests.Syntactical import TestSyntactical
from Tests.BinaryConversion import TestBinaryConversion
from Tests.LoadToRam import TestLoadToRam
from Tests.RunTimeMethods import TestRunTimeMethods
from Tests.SystemUAT import TestSystemUAT


unittest.main()
