"""The main project file where the translator is ran from"""
from tkinter import *
import src.GUIFuncs

gui_funcs = src.GUIFuncs

ROOT = Tk()

# Data about the current program
PROGRAM_DATA = {
    'ram': [],
    'registers': {
        'ACC': '0' * 16,
        'PC': 0,
        'input': None
    },
    'input_needed': False,
    'src_lines': [],
    'var_instructions': []
}

# Defining a common variable for all widgets needed by functions to go into
COMMON_WIDGETS = {}

# Defining top frames for the GUI
EDITTOR_FRAME = Frame(ROOT, padx=25, pady=25)
EDITTOR_FRAME.pack(side=LEFT)

EXECUTION_FRAME = Frame(ROOT, padx=25, pady=25)
EXECUTION_FRAME.pack(side=LEFT)

RAM_FRAME = Frame(ROOT, padx=25, pady=25)
RAM_FRAME.pack(side=LEFT)


# Adding the edittor widgets
Label(EDITTOR_FRAME, text='Code input').grid(row=0, column=0)
Button(EDITTOR_FRAME, text='Initialize to RAM', command=lambda: gui_funcs.generate_ram(PROGRAM_DATA, COMMON_WIDGETS)).grid(row=2, column=0)


# Making a textbox with a scrollbar
CODE_INPUT_FRAME = Frame(EDITTOR_FRAME)
CODE_INPUT_FRAME.grid(row=1, column=0)

CODE_INPUT_SCROLL = Scrollbar(CODE_INPUT_FRAME)

COMMON_WIDGETS['code_input'] = Text(CODE_INPUT_FRAME, height=20, width=25, yscrollcommand=CODE_INPUT_SCROLL.set)
COMMON_WIDGETS['code_input'].pack(side=LEFT)

CODE_INPUT_SCROLL.config(command=COMMON_WIDGETS['code_input'].yview)
CODE_INPUT_SCROLL.pack(side=LEFT, fill=Y)


# Adding the execution widgets
COMMON_WIDGETS['registers_var'] = StringVar()
COMMON_WIDGETS['registers_var'].set('ACC: 0\nPC: 0')
Message(EXECUTION_FRAME, textvariable=COMMON_WIDGETS['registers_var'], bg='#fff', relief=GROOVE).grid(row=0, column=0)
Button(EXECUTION_FRAME, text='Reset Registers', command=lambda: gui_funcs.reset_registers(PROGRAM_DATA, COMMON_WIDGETS)).grid(row=1, column=0)


# Adding the program output widgets
PROGRAM_OUTPUT = Frame(EXECUTION_FRAME, pady=5)
PROGRAM_OUTPUT.grid(row=2, column=0)

Label(PROGRAM_OUTPUT, text='Run-time Output').pack(side=TOP)

OUTPUT_SCROLL_X = Scrollbar(PROGRAM_OUTPUT, orient=HORIZONTAL)
OUTPUT_SCROLL_Y = Scrollbar(PROGRAM_OUTPUT, orient=VERTICAL)

COMMON_WIDGETS['output_box'] = Listbox(PROGRAM_OUTPUT, height=9, width=24, xscrollcommand=OUTPUT_SCROLL_X.set, yscrollcommand=OUTPUT_SCROLL_Y.set)
OUTPUT_SCROLL_X.config(command=COMMON_WIDGETS['output_box'].xview)
OUTPUT_SCROLL_Y.config(command=COMMON_WIDGETS['output_box'].yview)

OUTPUT_SCROLL_X.pack(side=BOTTOM, fill=X)
COMMON_WIDGETS['output_box'].pack(side=LEFT, fill=BOTH)
OUTPUT_SCROLL_Y.pack(side=LEFT, fill=Y)


# Adding the execution buttons for running the programs
EXECUTION_BUTTONS = Frame(EXECUTION_FRAME, pady=5)
EXECUTION_BUTTONS.grid(row=3, column=0)

COMMON_WIDGETS['execute_once'] = Button(EXECUTION_BUTTONS, text='Step Once', command=lambda: gui_funcs.step_once(PROGRAM_DATA, COMMON_WIDGETS))
COMMON_WIDGETS['execute_prog'] = Button(EXECUTION_BUTTONS, text='Execute Program', command=lambda: gui_funcs.rest_of_program(PROGRAM_DATA, COMMON_WIDGETS))
COMMON_WIDGETS['execute_once'].pack(side=LEFT)
COMMON_WIDGETS['execute_prog'].pack(side=LEFT)


# Adding the input field frame
INPUT_FIELD_FRAME = Frame(EXECUTION_FRAME, pady=5)
INPUT_FIELD_FRAME.grid(row=4, column=0)

Label(INPUT_FIELD_FRAME, text='Program Input').pack()
COMMON_WIDGETS['input_text'] = StringVar()
COMMON_WIDGETS['input_text_old'] = ''
COMMON_WIDGETS['input_text'].trace('w', lambda nm, idx, mode, var=COMMON_WIDGETS['input_text']: gui_funcs.input_field_validate(var, COMMON_WIDGETS))

COMMON_WIDGETS['input_field'] = Entry(INPUT_FIELD_FRAME, state=DISABLED, textvariable=COMMON_WIDGETS['input_text'])
COMMON_WIDGETS['input_field'].pack()

Button(INPUT_FIELD_FRAME, text='Submit', command=lambda: gui_funcs.input_field_button(PROGRAM_DATA, COMMON_WIDGETS)).pack()


# Adding the current line display widgets
CURR_LINE_OUTPUT = Frame(RAM_FRAME, pady=5)
CURR_LINE_OUTPUT.pack(side=TOP)

Label(CURR_LINE_OUTPUT, text='Current Line').pack(side=TOP)
COMMON_WIDGETS['curr_line_output'] = Text(CURR_LINE_OUTPUT, height=1, width=20)
COMMON_WIDGETS['curr_line_output'].config(state=DISABLED)
COMMON_WIDGETS['curr_line_output'].pack(side=BOTTOM)


# Adding the variable output widgets
VARIABLE_OUTPUT = Frame(RAM_FRAME, pady=5)
VARIABLE_OUTPUT.pack(side=TOP)

Label(VARIABLE_OUTPUT, text='Variable Values').pack(side=TOP)

VAR_SCROLL = Scrollbar(VARIABLE_OUTPUT)

COMMON_WIDGETS['variable_output'] = Listbox(VARIABLE_OUTPUT, height=5, width=16, yscrollcommand=VAR_SCROLL.set)
COMMON_WIDGETS['variable_output'].pack(side=LEFT, fill=BOTH)

VAR_SCROLL.config(command=COMMON_WIDGETS['output_box'].yview)
VAR_SCROLL.pack(side=LEFT, fill=Y)


# Adding the ram widgets
Label(RAM_FRAME, text='RAM Contents').pack()

COMMON_WIDGETS['ram_display_var'] = StringVar()
Message(RAM_FRAME, textvariable=COMMON_WIDGETS['ram_display_var'], bg='#fff', relief=GROOVE).pack(fill=BOTH)


ROOT.mainloop()
